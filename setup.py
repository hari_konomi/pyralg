try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup
    requires = install_requires

setup(
    name="pyralg",
    version="0.1.0",
    author="Theohar Konomi",
    author_email="konomi.theohar@gmail.com",
    packages=["pyralg"],
    include_package_data=True,
    package_data={'': ["LICENSE.txt", 'README.md']},
    url="https://bitbucket.org/hari_konomi/pyralg",
    license="MIT",
    description="Simple relational algebra package.",
    long_description=open("README.md").read(),
    # Dependent packages (distributions)
    install_requires=[
        "tabulate",
    ],
)
