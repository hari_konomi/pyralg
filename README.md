# pyralg #

pyralg is a python package that enables an easy, ORM-like interface to write and test relational algebra queries on different schemas, and their instances.

### Why pyralg? ###

While studying relational algebra for my databases course, it soon become quite tedious to write and test out long and complicated queries on paper.
Therefore I decided to write a package that would help me to test the queries much faster (and save paper as well!). 

### Version ###
* The current version is 0.1.0

### Features ###
* Basic relational algebra operations such as *projection*, *selection*, *rename*, *natural|theta join*, *Cartesian product*, *union*, *intersection*, *difference*.
* Primary keys and foreign keys defined for schemas.
* Different data types for instance values.
* Familiar ORM-like implementation of the operations.
* Method chaining for writing long and complicated queries, without having to store intermediate results.
* Each operation returns a 'snapshot' of the schema instance, instead of modifying it in place.

### How to get pyralg? ###

* Get the code
    * To get the latest version of the code, please git clone the repository, or download a zipped copy through the [Downloads](https://bitbucket.org/hari_konomi/pyralg/downloads) menu, and extract in a temporary directory.

* Install
    * `cd` into the directory you downloaded/extracted pyralg
    * run `python setup.py install`

* Dependencies
    * Python 2.7.6+
    * tabulate

### Examples ###
* Create schemas and define key constraints
```python

from pyralg.schema import Schema
# define a new schema named 'Student', that has 'sID', 'name', and 'lastName' as 
# attributes, of types int, str and str. (also check help(Schema) in a Python interpreter)
student = Schema('Student', ('sID', 'name', 'lastName'), (int, str, str))
# add a primary key on 'sID' for student
student.add_key(('sID',))
taken = Schema('Took', ('student', 'course', 'grade'), (int, int, int))
course = Schema('Course', ('cID', 'dept', 'code'), (int, str, str))
# add a foreign key for taken's 'student' to student's 'sID'
taken.add_foreign_key(student, {'student': 'sID'})
# add a foreign key for taken's 'course', to course's 'cID'
taken.add_foreign_key(course, {'course': 'cID'})
# add a primary key on 'cID', 'dept', 'code' for course
# actually, if no keys are specified the uniqueness of entries
# forces this constraint as well.
course.set_key(('cID', 'dept', 'code'))
```
* create instances of defined schemas
```python
# we will use the defined schemas in the previous example.
# get a 'singleton' instance of the student schema.
student_instance = student.initialize()
# add tuples to the instance (order of values should be the same as the 
# order of attributes in the schema)
student_instance.add_tuples([
            (1, 'John', 'Doe'),
            (2, 'Jane', 'Doe'),
        ])
# or a single tuple
student_instance.add_tuple((3, 'John', 'Smith'))

# initialize course and add values.
course_instance = course.initialize()
course_instance.add_tuples([
            (1, 'csc', '343'),
            (2, 'csc', '443'),
            (3, 'ece', '454')
        ])

# initialize taken and add values.
taken_instance = taken.initialize()
taken_instance.add_tuples([
            (1, 1, 90),
            (2, 1, 80),
            (3, 2, 85),
            (1, 2, 70),
            (2, 3, 69)
        ])
#trying to violate the constraint will raise an exception
taken_instance.add_tuple((4, 1, 50)) # this will throw an exception as there is no student with sID = 4.
```
* available operations
```python
# the above defined schemas and instances will be used
# rename the student instance and get a 'snapshot' of that instance with the new name/attributes
renamed_student_instance = student_instance.rename('NamedStudent', attributes=('studentID', 'fn', 'ln'))
#student_instance will remain unchanged

# join two instances with cartesian product (also use show() to print the newly created instance)
# the result will have a new name specified by the name argument passed to product
student_instance.product(taken_instance, name="result").show()
# the following will be printed
# result
# |   sID | name   | lastName   |   student |   course |   grade |
# |-------+--------+------------+-----------+----------+---------|
# |     1 | John   | Doe        |         2 |        1 |      80 |
# |     2 | Jane   | Doe        |         3 |        2 |      85 |
# |     3 | John   | Smith      |         2 |        1 |      80 |
# |     1 | John   | Doe        |         1 |        1 |      90 |
# |     3 | John   | Smith      |         1 |        1 |      90 |
# |     1 | John   | Doe        |         3 |        2 |      85 |
# |     2 | Jane   | Doe        |         2 |        1 |      80 |
# |     3 | John   | Smith      |         3 |        2 |      85 |
# |     2 | Jane   | Doe        |         2 |        3 |      69 |
# |     1 | John   | Doe        |         1 |        2 |      70 |
# |     3 | John   | Smith      |         1 |        2 |      70 |
# |     1 | John   | Doe        |         2 |        3 |      69 |
# |     3 | John   | Smith      |         2 |        3 |      69 |
# |     2 | Jane   | Doe        |         1 |        1 |      90 |
# |     2 | Jane   | Doe        |         1 |        2 |      70 |

# project on different columns in an instance
# rename the snapshot to "student ids" with one attribute called "studentID" which inherits the int type
student_instance.project(("sID",), name="student ids", attribute_names=("studentID",)).show()
# the following will be printed
# student ids
# |   studentID |
# |-------------|
# |           1 |
# |           2 |
# |           3 |

#project on name and lastName only but keep the name and attribute names (notice the order is changed)
student_instance.project(("lastName", "name")).show()
# the following will be printed
# Student
# | lastName   | name   |
# |------------+--------|
# | Doe        | John   |
# | Smith      | John   |
# | Doe        | Jane   |

# select rows based on a condition. 
# anonymous lambda functions can be used to express the condition. The function takes a parameter
# and returns a bool based on the function body statement. attributes of instances can be used
# as python attributes inside this function.
# select all students with sID = 1 or lastName = 'Doe', call the instance 'newname' and change the attributes
student_instance.select(lambda e: e.sID == 1 or e.lastName == 'Doe', name="newname", attribute_names=("stID", "fn", "ln")).show()
# prints the following
# newname
# |   stID | fn   | ln   |
# |--------+------+------|
# |      1 | John | Doe  |
# |      2 | Jane | Doe  |

# natural join two (or more) instances.
# we can see the function chaining by doing a rename and then the join.
taken_instance.rename('Took', attributes=('sID', 'cID', 'grade')).natural_join(student_instance) #use show() to print the result
# we can also keep joining with another instance by chaining.
taken_instance.rename('Took', attributes=('sID', 'cID', 'grade')).natural_join(student_instance).natural_join(course_instance).show()
# this prints the following (I didn't rename the final relation in this example so it concatenates the names)
# TookStudentCourse
# |   sID |   cID |   grade | name   | lastName   | dept   |   code |
# |-------+-------+---------+--------+------------+--------+--------|
# |     2 |     3 |      69 | Jane   | Doe        | ece    |    454 |
# |     1 |     1 |      90 | John   | Doe        | csc    |    343 |
# |     2 |     1 |      80 | Jane   | Doe        | csc    |    343 |
# |     1 |     2 |      70 | John   | Doe        | csc    |    443 |
# |     3 |     2 |      85 | John   | Smith      | csc    |    443 |

# perform a theta join (same as performing a product and then selecting on a condition)
student_instance.theta_join(taken_instance, lambda e: e.sID == e.student, name="Student grades")

# perform union, intersect, and difference. Same rules as in set operations hold!
does = student_instance.select(lambda e: e.lastName == 'Doe', name="Does") # get all the does
u = student_instance.union(does, name="union") # same as u = student_instance | does
i = student_instance.intersect(does, name="intersection") # same as i = student_instance & does
d = student_instance.difference(does, name="difference") # same as d = student_instance - does
```
* Extra features
``` python
# get the size of an instance
size = len(student_instance)
# iterate over the values of an instance (returned as tuples)
for tuple in student_instance:
    # do something with the tuple

# remove the instance from this schema
student.clear_instance()
```
### What if something is not working? ###

* Please feel free to submit a bug report or suggest a feature [here](https://bitbucket.org/hari_konomi/pyralg/issues).

### License ###
* pyralg is licensed under the MIT License.