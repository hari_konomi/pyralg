class Entry(object):
    """
    Entry(values, attributes, types=None)

    Parameters
    ----------
    values : An ordered tuple with the values.
    attributes : A tuple of strings representing the attributes for this entry.
                 note: attribute names should not contain spaces. 
    types : A tuple, representing the types of attributes for this entry.
            It defaults to type <str> for all the attributes.
            The order of types in the tuple should be the same as the order
            of attributes.
    """ 
    def __init__(self, values, attributes, types=None):
        if not isinstance(values, tuple):
            raise EntryException("values should be a tuple")
        for val in values:
            if not isinstance(val, (int, str, bool)):
                raise EntryException("value '{}' is not an integer, string or bool".format(val))
        self.values = values
        if not isinstance(attributes, tuple):
            raise EntryException("attributes should be a tuple")
        if len(values) != len(attributes):
            raise EntryException("mismatch in values and attributes length")
        self.types = types if types else tuple([str for _ in xrange(len(attributes))])
        for v, t, a in map(None, values, self.types, attributes):
            if not isinstance(v, t):
                raise EntryException("type mismatch: expected '{}' for attribute '{}', got '{}'".format(t, a, type(v)))
        self.attributes = attributes
        for v, a in map(None, values, attributes):
            setattr(self, a, v)

    def __hash__(self):
        vlen = len(self.values)
        entry_hash = 1
        for v in self.values:
            entry_hash += hash(v) * vlen
            vlen -= 1
        return entry_hash

    def __eq__(self, other):
        if self is other:
            return True
        svalues = self.values
        ovalues = other.get_values()
        if len(svalues) != len(ovalues):
            return False
        for i in xrange(len(svalues)):
            if svalues[i] != ovalues[i]:
                return False
        return True

    def __ne__(self, other):
        return not self == other

    def __str__(self):
        return "{}".format(self.values)

    def __repr__(self):
        return str(self)

    def get_values(self):
        """ Return the values of this entry """
        return self.values

class EntryException(Exception):

    def __init__(self, message):
        super(EntryException, self).__init__(message)