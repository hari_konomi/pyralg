from entry import Entry
try:
    from tabulate import tabulate
    TABULATE_AVAILABLE = True
except:
    TABULATE_AVAILABLE = False

class SchemaInstance(object):
    """
    SchemaInstance(name, attributes, key=None, types=None, fk=None)

    Parameters
    ----------
    name : A string representing this instance's name.
    attributes : A tuple of strings representing the attributes for this schema instance.
                 note: attribute names should not contain spaces. 
    key : A tuple representing the primary key constraints declared for the schema of this
          instance. Defaults to None
    types : A tuple, representing the types of attributes for this schema instance.
            It defaults to type <str> for all the attributes.
            The order of types in the tuple should be the same as the order
            of attributes.
    fk : A list of tuples representing the foreign key constraints declared for the schema
         of this instance. Defaults to None
    """ 
    def __init__(self, name, attributes, key=None, types=None, fk=None):
        self.name = name
        self.entries = set()
        self.attributes = attributes
        self.key = key
        self.types = types if types else tuple([str for _ in xrange(len(attributes))])
        self.fk = fk

    def __len__(self):
        return len(self.entries)

    def __str__(self):
        if TABULATE_AVAILABLE:
            return tabulate([e.get_values() for e in self.entries], headers=self.attributes, tablefmt='orgtbl')
        else:
            out = " |\t".join(self.attributes)
            out += "_" * len(out)
            for e in self.entries:
                out += " |\t".join([str(v) for v in e.get_values()])
            return out

    def __repr__(self):
        return "SchemaInstance({}, {}, key={}, types={}, fk={})".format(self.name, self.attributes, self.key, self.types, self.fk)

    def __iter__(self):
        for e in self.entries:
            yield e.get_values()

    def __and__(self, other):
        return self.intersect(other)

    def __or__(self, other):
        return self.union(other)

    def __sub__(self, other):
        return self.difference(other)

    def update_key(self, key):
        for en in self.entries:
            if all([getattr(en, k) == getattr(e, k) for k in key]):
                raise SchemaInstanceException("Integrity constraint captured on existing entries with new key.")
        self.key = key

    def update_foreign_key(self, foreign_key):
        for f in self.fk:
            osi = f[0].initialize()
            for k, v in f[1].items():
                for e in self.entries:
                    if getattr(e, k) not in [getattr(en, v) for en in osi.entries]:
                        raise SchemaInstanceException("Violation for foreign key constraint on existing entries for attribute '{}'".format(k))
        self.fk = foreign_key

    def add_tuple(self, value):
        """
        Adds a new tuple of values to the instance.
        Raises SchemaInstanceException if the tuple already exists, or an
        integrity violation happens.

        Parameters
        ----------
        value : A tuple of values to enter. The values should be in the
                same order as the attributes declared for the schema, 
                and have the same type as the types declared in the schema.

        Example
        -------
        s = Schema('Student', ('sID', 'name', 'lastName'), (int, str, str))
        s.set_key(('sID',))
        i = s.initialize() # get the instance
        i.add_tuple((1, 'John', 'Doe'))
        i.add_tuple((2, 'Jane', 'Doe'))
        i.add_tuple((1, 'John', 'Doe')) # will throw an exception, tuple already exists.
        i.add_tuple((2, 'John', 'Smith')) # will throw an exception, as sID is a primary key, and 2 is already used.
        """
        e = Entry(value, self.attributes, self.types)
        if e in self.entries:
            raise SchemaInstanceException("Tuple already exists in instance")
        for en in self.entries:
            if self.key:
                if all([getattr(en, k) == getattr(e, k) for k in self.key]):
                    raise SchemaInstanceException("Integrity constraint on primary key")

        if self.fk:
            for f in self.fk:
                osi = f[0].initialize()
                for k, v in f[1].items():
                    if getattr(e, k) not in [getattr(en, v) for en in osi.entries]:
                        raise SchemaInstanceException("Violation for foreign key constraint for attribute '{}'".format(k))
        self.entries.add(e)

    def add_tuples(self, values):
        """
        Adds tuples of values to the instance.
        Raises SchemaInstanceException if any of the tuples already exists, or an
        integrity violation happens.

        Parameters
        ----------
        values : A list of tuples of values to enter. The values should be in the
                same order as the attributes declared for the schema, 
                and have the same type as the types declared in the schema.

        Example
        -------
        s = Schema('Student', ('sID', 'name', 'lastName'), (int, str, str))
        s.set_key(('sID',))
        i = s.initialize() # get the instance
        i.add_tuples([
            (1, 'John', 'Doe'),
            (2, 'Jane', 'Doe'),
            (3, 'John', 'Smith')
        ])
        """
        for value in values:
            self.add_tuple(value)

    def show(self):
        """
        Print the entries of the instance in a formatted way.
        """
        print self.name
        if TABULATE_AVAILABLE:
            print tabulate([e.get_values() for e in self.entries], headers=self.attributes, tablefmt='orgtbl')
            print
        else:
            header = " |\t".join(self.attributes)
            print header
            print "_" * len(header)
            for e in self.entries:
                print " |\t".join([str(v) for v in e.get_values()])
            print

    def rename(self, name, attributes=None):
        """
        Return a snapshot of the instance, with a new name, and renamed attributes if defined.
        Doesn't modify the instance itself.

        Parameters
        ----------
        name : The new name for the snapshot
        attributes : New names for each of the attributes, if not specified the originals will be used.

        Example
        -------
        s = Schema('Student', ('sID', 'name', 'lastName'), (int, str, str))
        s.set_key(('sID',))
        i = s.initialize() # get the instance
        print i 
        #prints SchemaInstance('Student', ('sID', 'name', 'lastName'), key=('sID',), types=(int, str, str), fk=None)
        print i.rename('NamedStudent', attributes=('studentID', 'fn', 'ln'))
        #prints SchemaInstance('NamedStudent', ('studentID', 'fn', 'ln'), key=('sID',), types=(int, str, str), fk=None)
        print i
        #prints SchemaInstance('Student', ('sID', 'name', 'lastName'), key=('sID',), types=(int, str, str), fk=None)
        """
        if not attributes:
            attributes = self.attributes
        if len(attributes) != len(self.attributes):
            raise SchemaInstanceException("Attributes length different from original")
        new_entries = set()
        for oe in self.entries:
            ie = Entry(oe.get_values(), attributes, types=self.types)
            new_entries.add(ie)
        return SchemaInstanceSnapshot(name, attributes, new_entries, key=self.key, types=self.types)

    def product(self, other, name=""):
        """
        Perform a cartesian product between this instance and another instance.
        Return a newly created snapshot with the result of the product.
        Doesn't modify the instance itself.

        Parameters
        ----------
        other : A schema instance or a snapshot, to do the cartesian product with.
        name : A string for the newly returned snapshot. If not supplied, the new name
               will be the concatenation of the two instances' names.

        Example
        -------
        s = Schema('Student', ('sID', 'name', 'lastName'), (int, str, str))
        s.set_key(('sID',))
        i = s.initialize() # get the instance
        i.add_tuples([
            (1, 'John', 'Doe'),
            (2, 'Jane', 'Doe'),
            (3, 'John', 'Smith')
        ])
        d = Schema('Took', ('cID', 'stID', 'grade'), (int, int, int))
        d.set_foreign_key(s, {'stID':'sID'})
        di = d.initialize()
        di.add_tuples([
            (1, 1, 90),
            (2, 1, 80),
            (3, 2, 85),
            (1, 2, 70),
            (2, 3, 69)
        ])
        i.product(di, 'result').show()
        #prints the following
        result
        |   sID | name   | lastName   |   cID |   stID |   grade |
        |-------+--------+------------+-------+--------+---------|
        |     1 | John   | Doe        |     2 |      1 |      80 |
        |     2 | Jane   | Doe        |     3 |      2 |      85 |
        |     3 | John   | Smith      |     2 |      1 |      80 |
        |     1 | John   | Doe        |     1 |      1 |      90 |
        |     3 | John   | Smith      |     1 |      1 |      90 |
        |     1 | John   | Doe        |     3 |      2 |      85 |
        |     2 | Jane   | Doe        |     2 |      1 |      80 |
        |     3 | John   | Smith      |     3 |      2 |      85 |
        |     2 | Jane   | Doe        |     2 |      3 |      69 |
        |     1 | John   | Doe        |     1 |      2 |      70 |
        |     3 | John   | Smith      |     1 |      2 |      70 |
        |     1 | John   | Doe        |     2 |      3 |      69 |
        |     3 | John   | Smith      |     2 |      3 |      69 |
        |     2 | Jane   | Doe        |     1 |      1 |      90 |
        |     2 | Jane   | Doe        |     1 |      2 |      70 |

        """
        if not isinstance(other, (SchemaInstance, SchemaInstanceSnapshot)):
            raise SchemaInstanceException("Argument to product is not a schema instance")
        if any([k == v for k, v in map(None, self.attributes, other.attributes)]):
            raise SchemaInstanceException("Same attribute names for two different instances. \
Consider renaming an instance")
        new_attributes = self.attributes + other.attributes
        new_entries = set()
        for oe in self.entries:
            for ie in other.entries:
                ne = Entry(oe.get_values() + ie.get_values(), new_attributes, oe.types+ie.types)
                new_entries.add(ne)
        if not name:
            name = self.name+other.name
        return SchemaInstanceSnapshot(name, new_attributes, new_entries, types=self.types+other.types)

    def project(self, attributes, name="", attribute_names=None):
        """
        Project on this instance, and return a new snapshot with only the defined attributes.
        Duplicates will be removed.
        Doesn't modify the instance itself.

        Parameters
        ----------
        attributes : A tuple of strings, representing the attributes to project on.
        name : A new name for the returned instance snapshot. Defaults to the original one.
        attribute_names : A tuple of strings, representing new names for the projected attributes.
                          Defaults to the original names.

        Example
        -------
        s = Schema('Student', ('sID', 'name', 'lastName'), (int, str, str))
        s.set_key(('sID',))
        i = s.initialize() # get the instance
        i.add_tuples([
            (1, 'John', 'Doe'),
            (2, 'Jane', 'Doe'),
            (3, 'John', 'Smith')
        ])
        i.project(('sID',), name='student ids', attribute_names=('studentID',)).show()
        #prints the following
        student ids
        |   studentID |
        |-------------|
        |           1 |
        |           2 |
        |           3 |

        i.project(('name',), name='student names', attribute_names=('studentame',)).show()
        #prints the following
        student names
        | studentname   |
        |---------------|
        | Jane          |
        | John          |

        """
        if not all(k in self.attributes for k in attributes):
            raise SchemaInstanceException("Attribute(s) missing in original instance.")
        if not attribute_names:
            attribute_names = attributes
            
        new_entries = set()
        types = []
        for a in attributes:
            if a in self.attributes:
                types.append(self.types[self.attributes.index(a)])
        types = tuple(types)

        for e in self.entries:
            ne = Entry(tuple([getattr(e, k) for k in attributes]), attribute_names, types)
            new_entries.add(ne)
        if not name:
            name = self.name
        return SchemaInstanceSnapshot(name, attribute_names, new_entries, types=types)

    def select(self, condition, name="", attribute_names=None):
        """
        Perform a select on the instance, returning a new snapshot with
        only the tuples that pass the test in condition.
        Doesn't modify the instance itself.

        Parameters
        ----------
        condition : A function taking an Entry, and returning true or false, if
                    a condition is passed or failed.
                    It is suggested to use a lambda function (see example), and
                    attributes can be accessed by name as a python class attribute.
        name : A new name for the returned instance snapshot. Defaults to the original one.
        attribute_names : A tuple of strings, representing new names for the projected attributes.
                          Defaults to the original names.

        Example
        -------
        s = Schema('Student', ('sID', 'name', 'lastName'), (int, str, str))
        s.set_key(('sID',))
        i = s.initialize() # get the instance
        i.add_tuples([
            (1, 'John', 'Doe'),
            (2, 'Jane', 'Doe'),
            (3, 'John', 'Smith')
        ])

        i.select(lambda e: e.name == 'John', name='JohnStudents').show()
        # prints the following
        JohnStudents
        |   sID | name   | lastName   |
        |-------+--------+------------|
        |     1 | John   | Doe        |
        |     3 | John   | Smith      |

        i.select(lambda e: e.name != 'John', name="NotJohnStudents").show()
        NotJohnStudents
        |   sID | name   | lastName   |
        |-------+--------+------------|
        |     2 | Jane   | Doe        |
        """
        new_entries = set()
        if not attribute_names:
            attribute_names = self.attributes
        if len(attribute_names) != len(self.attributes):
            raise SchemaInstanceException("Unequal number of attributes.")
        for e in self.entries:
            if condition(e):
                new_entries.add(e)
        if not name:
            name = self.name
        return SchemaInstanceSnapshot(name, attribute_names, new_entries, types=self.types)

    def natural_join(self, other, name=""):
        """
        Perform a natural join on the instance with another one. 
        Return a new snapshot with the result.
        Doesn't modify the instance itself.

        Parameters
        ----------
        other : An instance to do the join with.
        name : A new name for the returned instance snapshot. Defaults to the concatenation
               of the two instances names.

        Example
        -------
        s = Schema('Student', ('sID', 'name', 'lastName'), (int, str, str))
        s.set_key(('sID',))
        i = s.initialize() # get the instance
        i.add_tuples([
            (1, 'John', 'Doe'),
            (2, 'Jane', 'Doe'),
            (3, 'John', 'Smith')
        ])
        d = Schema('Took', ('cID', 'sID', 'grade'), (int, int, int))
        d.set_foreign_key(s, {'sID':'sID'})
        di = d.initialize()
        di.add_tuples([
            (1, 1, 90),
            (2, 1, 80),
            (3, 2, 85),
            (1, 2, 70),
            (2, 3, 69)
        ])
        i.natural_join(di, name="Student grades").show()
        #prints the following
        Student grades
        |   sID | name   | lastName   |   cID |   grade |
        |-------+--------+------------+-------+---------|
        |     3 | John   | Smith      |     2 |      69 |
        |     1 | John   | Doe        |     1 |      90 |
        |     2 | Jane   | Doe        |     3 |      85 |
        |     1 | John   | Doe        |     2 |      80 |
        |     2 | Jane   | Doe        |     1 |      70 |

        """
        if not isinstance(other, (SchemaInstance, SchemaInstanceSnapshot)):
            raise SchemaInstanceException("Argument to join is not a schema instance")
        new_attributes = self.attributes + tuple([k for k in other.attributes if k not in self.attributes])

        common = [a for a in self.attributes if a in other.attributes]
        if not common: #this is just a product then.
            return self.product(other, name=name)

        new_entries = set()
        types = self.types + tuple([other.types[i] for i in xrange(len(other.attributes)) if other.attributes[i] not in self.attributes])

        for oe in self.entries:
            for ie in other.entries:
                if all(getattr(oe, k) == getattr(ie, k) for k in common):
                    vals = oe.get_values() + tuple([getattr(ie, k) for k in other.attributes if k not in self.attributes])
                    ne = Entry(vals, new_attributes, types)
                    new_entries.add(ne)

        if not name:
            name = self.name + other.name
        return SchemaInstanceSnapshot(name, new_attributes, new_entries, types=types)

    def theta_join(self, other, condition, name=""):
        """
        Perform a theta join on this instance with another one.
        Same as doing a cartesian product with the instance, and then doing a select.
        Return a new snapshot with the result.
        Doesn't modify the instance itself.

        Parameters
        ----------
        other : An instance to do the theta join with.
        condition : A function taking an Entry, and returning true or false, if
                    a condition is passed or failed.
                    It is suggested to use a lambda function (see example), and
                    attributes can be accessed by name as a python class attribute.
        name : A new name for the returned instance snapshot. Defaults to the concatenation
               of the two instances names.

        Example
        -------
        s = Schema('Student', ('sID', 'name', 'lastName'), (int, str, str))
        s.set_key(('sID',))
        i = s.initialize() # get the instance
        i.add_tuples([
            (1, 'John', 'Doe'),
            (2, 'Jane', 'Doe'),
            (3, 'John', 'Smith')
        ])
        d = Schema('Took', ('cID', 'stID', 'grade'), (int, int, int))
        d.set_foreign_key(s, {'stID':'sID'})
        di = d.initialize()
        di.add_tuples([
            (1, 1, 90),
            (2, 1, 80),
            (3, 2, 85),
            (1, 2, 70),
            (2, 3, 69)
        ])
        i.theta_join(di, lambda e: e.sID == e.stID, name="Student grades").show()
        #prints the following
        Student grades
        |   sID | name   | lastName   |   cID |   stID |   grade |
        |-------+--------+------------+-------+--------+---------|
        |     1 | John   | Doe        |     2 |      1 |      80 |
        |     2 | Jane   | Doe        |     3 |      2 |      85 |
        |     3 | John   | Smith      |     2 |      3 |      69 |
        |     2 | Jane   | Doe        |     1 |      2 |      70 |
        |     1 | John   | Doe        |     1 |      1 |      90 |

        # To remove the stID, we can do a project.
        i.theta_join(di, lambda e: e.sID == e.stID, name="Student grades").project(('sID', 'cID', 'name', 'lastName', 'grade')).show()
        # prints the following
        Student grades
        |   sID |   cID | name   | lastName   |   grade |
        |-------+-------+--------+------------+---------|
        |     2 |     3 | Jane   | Doe        |      85 |
        |     2 |     1 | Jane   | Doe        |      70 |
        |     1 |     2 | John   | Doe        |      80 |
        |     1 |     1 | John   | Doe        |      90 |
        |     3 |     2 | John   | Smith      |      69 |

        """
        return self.product(other, name=name).select(condition)

    def union(self, other, name=""):
        """
        Return a new snapshot of the union between this instance and another instance.
        Doesn't modify the instance itself.

        Parameters
        ----------
        other : An instance to do the union with.
        name : A new name for the returned instance snapshot. Defaults to i1.name|i2.name
        """
        if not isinstance(other, (SchemaInstance, SchemaInstanceSnapshot)):
            raise SchemaInstanceException("Argument to union is not a schema instance.")
        if self.attributes != other.attributes:
            raise SchemaInstanceException("Different attributes for the instances.")
        if self.types != other.types:
            raise SchemaInstanceException("Different types for the instances.")
        if not name:
            name = "|".join([self.name, other.name])
        return SchemaInstanceSnapshot(name, self.attributes, self.entries|other.entries, types=self.types)

    def intersect(self, other, name=""):
        """
        Return a new snapshot of the intersection between this instance and another instance.
        Doesn't modify the instance itself.

        Parameters
        ----------
        other : An instance to do the intersection with.
        name : A new name for the returned instance snapshot. Defaults to i1.name&i2.name
        """
        if not isinstance(other, (SchemaInstance, SchemaInstanceSnapshot)):
            raise SchemaInstanceException("Argument to intersect is not a schema instance.")
        if self.attributes != other.attributes:
            raise SchemaInstanceException("Different attributes for the instances.")
        if self.types != other.types:
            raise SchemaInstanceException("Different types for the instances.")
        if not name:
            name = "&".join([self.name, other.name])
        return SchemaInstanceSnapshot(name, self.attributes, self.entries&other.entries, types=self.types)

    def difference(self, other, name=""):
        """
        Return a new snapshot of the difference between this instance and another instance.
        Doesn't modify the instance itself.

        Parameters
        ----------
        other : An instance to do the subtraction with.
        name : A new name for the returned instance snapshot. Defaults to i1.name-i2.name
        """
        if not isinstance(other, (SchemaInstance, SchemaInstanceSnapshot)):
            raise SchemaInstanceException("Argument to difference is not a schema instance.")
        if self.attributes != other.attributes:
            raise SchemaInstanceException("Different attributes for the instances.")
        if self.types != other.types:
            raise SchemaInstanceException("Different types for the instances.")
        if not name:
            name = "-".join([self.name, other.name])
        return SchemaInstanceSnapshot(name, self.attributes, self.entries-other.entries, types=self.types)


class SchemaInstanceSnapshot(SchemaInstance):

    def __init__(self, name, attributes, entries, key=None, types=None, fk=None):
        self.name = name
        self.entries = frozenset(entries)
        self.attributes = attributes
        self.key = key
        self.types = types if types else tuple([str for _ in xrange(len(attributes))])
        self.fk = fk

    def __repr__(self):
        return "SchemaInstanceSnapshot({}, {}, key={}, types={}, fk={})"\
                .format(self.name, self.attributes, self.key, self.types, self.fk)

    def add_tuples(self, values):
        raise SchemaInstanceException("Cannot add tuples to a snapshot.")

    def add_tuple(self, value):
        raise SchemaInstanceException("Cannot add tuple to a snapshot.")

class SchemaInstanceException(Exception):

    def __init__(self, message):
        super(SchemaInstanceException, self).__init__(message)
