from instance import SchemaInstance, SchemaInstanceSnapshot

class Schema(object):
    """
    Schema(name, attributes, types=None)

    Parameters
    ----------
    name : A string representing the schema name.
    attributes : A tuple of strings representing the attributes for this schema.
                note: attribute names should not contain spaces. 
    types : A tuple, representing the types of attributes for this schema.
            It defaults to type <str> for all the attributes.
            The order of types in the tuple should be the same as the order
            of attributes.
    """
    def __init__(self, name, attributes, types=None):

        if not isinstance(name, str):
            raise SchemaException("name should be a string.")
        self.name = name
        if not isinstance(attributes, tuple):
            raise SchemaException("attributes should be a tuple of strings.")
        for a in attributes:
            if len(a.split()) > 1:
                raise SchemaException("attribute name can't contain whitespace.")
        temp = tuple([str(a) for a in attributes])
        self.attributes = temp
        self.key = None
        self.instance = None
        self.types = types if types else tuple([str for _ in xrange(len(attributes))])
        if len(self.types) != len(self.attributes):
            raise SchemaException("difference in length of attributes and types.")
        self.foreign_key = None

    def __str__(self):
        return "{}{}".format(self.name, self.attributes)

    def __repr__(self):
        return "{}{}".format(self.name, self.attributes)

    def add_key(self, key):
        """
        Add a key constraint to the schema.
        If a key already exists, this method appends to that key.
        If an instance for this schema has already been created,
        its key will get updated as well. 
        If the instance already has entries that violate the constraint,
        an exception will be thrown and the instance will not get updated.

        Parameters
        ----------
        key : A tuple of string(s) defining the primary key constraints, from 
              the schema attributes.

        Example
        -------
        s = Schema('Student', ('sID', 'name', 'lastName'), (int, str, str))
        s.add_key(('sID',))
        s.add_key(('name',)) # primary key will now be ('sID', 'name')
        # equivalent to s.add_key(('sID', 'name'))
        """
        if not isinstance(key, tuple):
            raise SchemaException("key should be a tuple of strings.")
        temp = tuple([str(a) for a in key])
        for key in temp:
            if key not in self.attributes:
                raise SchemaException("Unknown attribute: {}".format(key))
        if not self.key:
            self.key = temp
        else:
            self.key += temp
        if self.instance:
            self.instance.update_key(self.key)

    def set_key(self, key):
        """
        Set a key constraint to the schema.
        If a key already exists, this method replaces that key.
        If an instance for this schema has already been created,
        its key will get updated as well. 
        If the instance already has entries that violate the constraint,
        an exception will be thrown and the instance will not get updated.

        Parameters
        ----------
        key : A tuple of string(s) defining the primary key constraints, from 
              the schema attributes.

        Example
        -------
        s = Schema('Student', ('sID', 'name', 'lastName'), (int, str, str))
        s.set_key(('sID',)) # primary key is now ('sID',)
        s.set_key(('name',)) # primary key is now ('name',)
        s.set_key(('sID', 'name')) #primary key is now ('sID', 'name')
        """
        if not isinstance(key, tuple):
            raise SchemaException("key should be a tuple of strings.")
        temp = tuple([str(a) for a in key])
        self.key = temp
        if self.instance:
            self.instance.update_key(self.key)

    def set_foreign_key(self, foreign_schema, foreign_key):
        """
        Set a foreign key constraint for the schema, to another schema.
        If a key already exists, this method replaces that key.
        If an instance for this schema has already been created,
        its key will get updated as well. 
        If the instance already has entries that violate the constraint,
        an exception will be thrown and the instance will not get updated.

        Parameters
        ----------
        foreign_schema : A schema object, to which the foreign key will referr to.
        foreign_key : a dictionary, with keys being attributes of the 'self' schema,
                      and corresponding values from the foreign_schema.

        Example
        -------
        s = Schema('Student', ('sID', 'name', 'lastName'), (int, str, str))
        t = Schema('Took', ('cID', 'student', 'grade'), (int, int, int))
        t.set_foreign_key(s, {'student', 'sID'})
        """
        if not isinstance(foreign_schema, Schema):
            raise SchemaException("foreign_schema should be a Schema object.")
        if not isinstance(foreign_key, dict):
            raise SchemaException("foreign_key should be a dict of paired attributes.")
        for k, v in foreign_key.items():
            if k not in self.attributes:
                raise SchemaException("unknown attribute '{}' for schema '{}'".format(k, self.name))
            if v not in foreign_schema.attributes:
                raise SchemaException("unknown attribute'{}' for schema '{}'".format(v, foreign_schema.get_name()))
        self.foreign_key = [(foreign_schema, foreign_key)]
        if self.instance:
            self.instance.update_foreign_key(self.foreign_key)

    def add_foreign_key(self, foreign_schema, foreign_key):
        """
        Add a foreign key constraint for the schema, to another schema.
        If a key already exists, this method appends to that key.
        If an instance for this schema has already been created,
        its key will get updated as well. 
        If the instance already has entries that violate the constraint,
        an exception will be thrown and the instance will not get updated.

        Parameters
        ----------
        foreign_schema : A schema object, to which the foreign key will referr to.
        foreign_key : a dictionary, with keys being attributes of the 'self' schema,
                      and corresponding values from the foreign_schema.

        Example
        -------
        s = Schema('Student', ('sID', 'name', 'lastName'), (int, str, str))
        t = Schema('Took', ('course', 'student', 'grade'), (int, int, int))
        c = Schema('Course', ('cID', 'dept', 'code'), (int, str, str))
        t.set_foreign_key(s, {'student':'sID'})
        t.add_foreign_key(c, {'course': 'cID'}) # Now t has a foreign key to s and to c.
        """
        if not isinstance(foreign_schema, Schema):
            raise SchemaException("foreign_schema should be a Schema object.")
        if not isinstance(foreign_key, dict):
            raise SchemaException("foreign_key should be a dict of paired attributes.")
        for k, v in foreign_key.items():
            if k not in self.attributes:
                raise SchemaException("unknown attribute '{}' for schema '{}'".format(k, self.name))
            if v not in foreign_schema.attributes:
                raise SchemaException("unknown attribute'{}' for schema '{}'".format(v, foreign_schema.name))
        if self.foreign_key:
            self.foreign_key.append((foreign_schema, foreign_key))
        else:
            self.foreign_key = [(foreign_schema, foreign_key)]
        if self.instance:
            self.instance.update_foreign_key(self.foreign_key)

    def get_name(self):
        """ Return the name of the schema"""
        return self.name

    def initialize(self):
        """ 
        Create and return a new schema instance to which data can be entered.
        If the reference to the instance is lost, calling initialize again will
        return the same existing instance.
        """
        if not self.instance:
            self.instance = SchemaInstance(self.name, self.attributes, key=self.key, types=self.types, fk=self.foreign_key)
        return self.instance

    def clear_instance(self):
        """Detach the instance from this schema"""
        self.instance = None

class SchemaException(Exception):

    def __init__(self, message):
        self.message = message
        super(SchemaException, self).__init__(message)
